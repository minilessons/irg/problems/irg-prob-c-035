/**
 * Gets time unit for pixel value
 *
 * @param s pixel value
 * @param value time unit of pixel value
 */
function getTimeUnitEasy(s, value) {
    return getTimeUnit(value, s.constants.numeric.timeUnit, s.constants.numeric.pixelUnit);
}


/**
 * Gets time unit for a pixel value.
 *
 * @param pixelValue number of pixels to convert
 * @param timeUnit time constants
 * @param pixelUnit pixel constant
 * @returns {number} time unit for given pixels
 */
function getTimeUnit(pixelValue, timeUnit, pixelUnit) {
    let pixelFactor = pixelValue / pixelUnit;
    return Math.floor(pixelFactor * timeUnit);
}


/**
 * Gets pixel unit for time value
 *
 * @param s time value
 * @param value pixel unit of time value
 */
function getPixelUnitEasy(s, value) {
    return getPixelUnit(value, s.constants.numeric.timeUnit, s.constants.numeric.pixelUnit)
}


/**
 * Gets pixel unit for a time value.
 *
 * @param timeValue number of pixels to convert
 * @param timeUnit time constants
 * @param pixelUnit pixel constant
 * @returns {number} pixel unit for given time units
 */
function getPixelUnit(timeValue, timeUnit, pixelUnit) {
    let timeFactor = timeValue / timeUnit;
    return Math.floor((timeFactor * pixelUnit));
}
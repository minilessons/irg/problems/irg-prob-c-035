/**
 * Main prototypes for CanvasX.
 * Initializes CanvasX prototypes before their usage.
 */
function initPrototypes() {
    /**
     * Redraws whole canvas.
     */
    CanvasX.prototype.invalidate = function () {
        this.valid = false;
        this.draw();
    };

    /**
     * Setter for canvas locale.
     *
     * @param ID locale ID
     */
    CanvasX.prototype.setLocale = function (ID) {
        this.locale.settings.ID = ID;
    };

    /**
     * Canvas locale.
     * @returns {*} locale object
     */
    CanvasX.prototype.getLocale = function () {
        return this.constants.strings.locale;
    };

    /**
     * Canvas reset to original clear state.
     * @param state
     */
    CanvasX.prototype.resetCanvas = function (state) {
        this.draggX = 0;
        this.draggY = 0;
        this.dragg = false;

        this.valid = false;
        this.shapes = [];
        this.xOptions.initialSetup = state;
        this.invalidate();
    };

    /**
     * Function for exporting canvasState.
     * @returns {{first, second}|*} list of first and second buffer containing information about added frames
     */
    CanvasX.prototype.exportState = function () {
        // Gather buffer frames, convert them to time domain and return their lists
        return convertToTime(this);

    };

    /**
     * Adds a shape to canvas.
     *
     * @param shape specified shape
     */
    CanvasX.prototype.addShape = function (shape) {
        this.shapes.push(shape);
    };

    /**
     * Clears all canvas drawings on context which canvas represents.
     */
    CanvasX.prototype.clear = function () {
        this.ctx.save();
        this.ctx.clearRect(0, 0, this.width, this.height);
        this.ctx.fillStyle = this.constants.colors.canvasBackground;
        this.ctx.fillRect(0, 0, this.width, this.height);
        this.ctx.restore();
    };

    /**
     * Clears shapes array.
     */
    CanvasX.prototype.removeShapes = function () {
        this.shapes = [];
        this.invalidate();
    };

    /**
     * Applies predicate to shape objects.
     *
     * @param shapeID object to apply to
     * @param predicate function to call for each shape with given ID
     */
    CanvasX.prototype.doOnShape = function (shapeID, predicate) {
        this.getShapesByID(shapeID).forEach(predicate);
    };

    /**
     * Checks if canvas contains point provided.
     *
     * @param mx x coordinate of a point
     * @param my y coordinate of a point
     * @returns {boolean} true if canvas contains point
     */
    CanvasX.prototype.contains = function (mx, my) {
        let rect = this.canvas.getBoundingClientRect();
        return ((mx < this.width) && (mx > rect.left) && (my > rect.top) && (my < rect.height));
    };


    /**
     * Draws all specified elements in this canvas. Only if they are not up to date.
     */
    CanvasX.prototype.draw = function () {
        if (!this.valid) {
            let ctx = this.ctx;
            let shapes = this.shapes;
            this.clear();

            let l = shapes.length;
            for (let c = 0; c < l; c++) {
                let sh = shapes[c];
                if (sh.isVisible === false) {
                    continue;
                }

                if (sh.transform !== undefined) {
                    ctx.translate(sh.transform.translation.x, sh.transform.translation.y);
                    ctx.save();
                    ctx.beginPath();
                    sh.draw(ctx);
                    ctx.restore();
                    ctx.translate(-sh.transform.translation.x, -sh.transform.translation.y);
                } else {
                    ctx.save();
                    ctx.beginPath();
                    sh.draw(ctx);
                    ctx.restore();

                }
            }
            this.valid = true;
        }
    };

    /**
     *  Gets a shape by ID (only one)
     *
     * @param ID id of a shape
     * @returns {shape} returns first found shape by specified ID
     */
    CanvasX.prototype.getShapeByID = function (ID) {
        return this.getShapesByID(ID)[0];
    };

    /**
     * Removes shapes by ID.
     *
     * @param ID id of all shapes to remove
     */
    CanvasX.prototype.removeShapesByID = function (ID) {
        let shapesToRemove = this.getShapesByID(ID);

        shapesToRemove.forEach((shape) => {
            this.removeShapeByID(shape.ID);
        });
    };

    /**
     * Removes shape object by id
     *
     * @param ID id of an object to remove
     */
    CanvasX.prototype.removeShapeByID = function (ID) {
        let index = this.shapes.indexOf(this.getShapeByID(ID));
        if (index > -1) {
            this.shapes.splice(index, 1);
        } else {
            /* throw "Nothin found for ID: " + ID;*/
        }
    };

    /**
     * Gets shapes by group identification
     *
     * @param groupID group identification
     * @returns {Array} list of grouped shapes by given identification
     */
    CanvasX.prototype.getShapesByGroupID = function (groupID) {
        let shapes = this.shapes;
        let retStream = [];
        shapes.forEach((shape) => {

            if (shape.groupID === groupID) {
                retStream.push(shape)
            }
        });
        return retStream;
    };

    /**
     * Gets shapes by identification
     *
     * @param ID identification
     * @returns {Array} list of all shapes which has specified ID
     */
    CanvasX.prototype.getShapesByID = function (ID) {
        let shapes = this.shapes;
        let retStream = [];
        shapes.forEach((shape) => {
            if (shape.ID !== undefined) {
                if (shape.ID === ID) {
                    retStream.push(shape);
                }
            }
        });
        return retStream;
    };

    /**
     * Adds canvas interaction but without options for changing state provided. Just mouse move and selector events.
     *
     * @param myState
     */
    CanvasX.prototype.addStaticEventListeners = function (myState) {
        this.canvas.addEventListener("selectstart", function (e) {
            e.preventDefault();
        }, false);
        this.canvas.addEventListener("contextmenu", function (e) {
            e.preventDefault();
            return false;
        }, false);
        this.canvas.addEventListener("mousemove", mouseMoveCallback, false);


        function mouseMoveCallback(e) {
            mouseMoved(e, myState);
        }
    };

    /**
     * Adds canvas interaction.
     *
     * @param myState reference to canvasX
     */
    CanvasX.prototype.addEventListeners = function (myState) {
        // Event listeners
        this.canvas.addEventListener("selectstart", function (e) {
            e.preventDefault();
        }, false);
        this.canvas.addEventListener("contextmenu", function (e) {
            e.preventDefault();
            return false;
        }, false);

        // Mouse events
        this.canvas.addEventListener("mousedown", mouseDownCallback, true);
        this.canvas.addEventListener("mousemove", mouseMoveCallback, false);
        this.canvas.addEventListener("mouseup", mouseUpCallback, false);


        function mouseDownCallback(e) {
            mouseClick(e, myState);
        }


        function mouseMoveCallback(e) {
            mouseMoved(e, myState);
        }


        function mouseUpCallback(e) {
            mouseReleased(e, myState);
        }


        // Keyboard events
        this.canvas.addEventListener("keydown", keyDownCallback, false);
        this.canvas.addEventListener("keypress", keyPressCallback, false);


        function keyDownCallback(e) {
            keyDownPress(e, myState);
        }


        function keyPressCallback(e) {
            keyPress(e, myState);
        }
    }
}


/**
 * Gets mouse coordinates
 *
 * @param canvas canvas in which mouse is at
 * @param e event object
 * @returns object which holds x and y position of current mouse location
 */
function getMouse(canvas, e) {
    let rect = canvas.getBoundingClientRect();
    let pos = {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };

    return {x: pos.x, y: pos.y}
}


initPrototypes();

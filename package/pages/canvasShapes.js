/**
 * Object transform: scale
 *
 * @param x horizontal scale value
 * @param y verticals scale value
 * @constructor
 */
function ScaleTransform(x, y) {
    this.x = x || 1;
    this.y = y || 1;

}


/**
 * Object transform: translate
 *
 * @param x horizontal translation value
 * @param y vertical translation value
 * @constructor
 */
function TranslateTransform(x, y) {
    this.x = x || 0;
    this.y = y || 0;

}


/**
 * Object transform: rotation
 *
 * @param angle rotation angle
 * @param CCW direction
 * @constructor
 */
function RotationTransform(angle, CCW) {
    this.CCW = CCW || false;
    this.angle = angle || 0;

    this.setAngle = function (value) {
        if (value >= 360) {
            this.angle = value % 360;
        } else if (value < 0) {
            let times = Math.floor(Math.abs(value) / 360);
            this.angle = (value + (times + 1) * 360);
        } else {
            this.angle = value;
        }
    };
}


/**
 * Transform for any shape object.
 *
 * @param scale scale transform
 * @param translation translate transform
 * @param rotation rotation transform
 * @constructor
 */
function Transform(scale, translation, rotation) {
    this.scale = scale || {x: 0, y: 0};
    this.rotation = rotation || {angle: 0, CCW: false};
    this.translation = translation || {x: 0, y: 0};

    this.getDefaultTransform = function () {
        return new Transform(new ScaleTransform(1, 1), new TranslateTransform(0, 0), new RotationTransform(0, false));
    }
}


/**
 * Function constructor which initializes a Shape.
 *
 * @param x top left x coordinate of a shape
 * @param y top left y coordinate of a shape
 * @param w width of the shape
 * @param h height of the shape
 * @param fill shape fill
 * @param selected simulates selection of this shape
 * @param ID object identification, for complex objects same ID is given
 * @param isStroke if shape stroke
 * @param rotX x coordinate of shape rotation point
 * @param rotY y coordinate of shape rotation point
 * @constructor
 */
function Shape(x, y, w, h, fill, selected, ID, isStroke, rotX, rotY) {
    this.ID = ID || undefined;
    this.x = x || 0;
    this.y = y || 0;
    this.w = w || 1;
    this.h = h || 1;
    this.fill = fill || '#AAAAAA';
    this.selected = selected || false;
    this.selectionColor = "#4123A4";
    this.isStroke = isStroke || false;
    this.rotX = rotX || 0;
    this.rotY = rotY || 0;
    this.transform = (new Transform()).getDefaultTransform();

    this.getRotPoint = function () {
        return {x: this.rotX, y: this.rotY};
    };

    // Visibility
    this.isVisible = true;
}


/**
 * Function constructor for MyButton shape.
 *
 * @param ID identification
 * @param x x coordinate of top left rectangle point
 * @param y y coordinate of top left rectangle point
 * @param w width of a shape
 * @param h height of a shape
 * @param text text inside shape
 * @param options other options
 * @constructor
 */
function MyButton(ID, x, y, w, h, text, options) {
    this.ID = ID || undefined;
    this.groupID = undefined;
    this.x = x || 0;
    this.y = y || 0;
    this.w = w || 1;
    this.h = h || 1;
    this.fillStyle = options.fillStyle || "#0AAAAA";
    this.alphaNonTransparent = options.alphaNonTransparent || 1;
    this.alphaTransparent = options.alphaTransparent || 0.5;
    this.strokeStyle = options.strokeStyle || "#000000";
    this.text = text || "";
    this.textColor = options.textColor || "#000000";
    this.textMaxSpan = options.textMaxSpan || 50;
    this.font = options.font;
    this.shadowBlur = options.shadowBlur || 0;
    this.shadowColor = options.shadowColor || "#000000";
    this.shadowOffsetX = options.shadowOffsetX || 0;
    this.shadowOffsetY = options.shadowOffsetY || 0;
    this.textBaseline = options.textBaseline || "middle";
    this.textHeight = Number((options.font.split("px")[0]));

    // Stroke and fill options
    this.fillShape = (options.fillShape === undefined) ? true : options.fillShape;
    this.strokeShape = (options.strokeShape === undefined) ? true : options.strokeShape;

    // Time units
    this.timeWidth = null;
    this.time = {
        startValue: 0,
        endValue: 1
    };

    // Frame selections
    this.frameSelectionColor = options.frameSelectionColor || "#00AAAA";

    // Curved rectangle buttons
    this.curved = options.curved || false;
    this.leftCurve = options.leftCurve || 12;
    this.rightCurve = options.rightCurve || 12;

    // Save options as structure
    this.options = options;

    // Selection options
    this.selected = false;
    this.isHovered = false;
    this.isPressed = false;

    // Visibility
    this.isVisible = true;

    // Transformations
    this.transform = new Transform().getDefaultTransform();

}


/**
 * Function constructor for line shape.
 *
 * @param x1 start x value of this line
 * @param y1 start y value of this line
 * @param x2 end x value of this line
 * @param y2 end y value of this line
 * @param color line color
 * @param fill line fill
 * @param lineWidth width of the line
 * @param rotX x coordinate of shape rotation point
 * @param rotY y coordinate of shape rotation point
 * @constructor
 */
function Line(x1, y1, x2, y2, color, fill, lineWidth, rotX, rotY) {
    this.x1 = x1 || 0;
    this.x2 = x2 || 0;
    this.y1 = y1 || 1;
    this.y2 = y2 || 1;
    this.color = color || "#000000";
    this.fill = fill || "#000000";
    this.lineWidth = lineWidth || 1;
    this.rotX = rotX || 0;
    this.rotY = rotY || 0;
    this.transform = (new Transform()).getDefaultTransform();

    this.getRotPoint = function () {
        return {x: this.rotX, y: this.rotY};
    };

    // Visibility
    this.isVisible = true;
}


/**
 * Function constructor for DLine shape.
 *
 * @param ID identification
 * @param x1 x coordinate of start point
 * @param y1 y coordinate of start point
 * @param x2 x coordinate of end point
 * @param y2 y coordinate of end point
 * @param opts other options
 * @param groupID group identification
 * @constructor
 */
function DLine(ID, x1, y1, x2, y2, opts, groupID) {
    this.ID = ID || undefined;
    this.groupID = groupID || undefined;
    this.x1 = x1 || 0;
    this.x2 = x2 || 0;
    this.y1 = y1 || 1;
    this.y2 = y2 || 1;
    this.fillStyle = opts.fillStyle || "#000000";
    this.lineWidth = opts.lineWidth || 1;
    this.lineDash = opts.lineDash || [];

    // Visibility
    this.isVisible = true;
}


/**
 * Function constructor for text field.
 *
 * @param ID object identification string
 * @param text text which is shown in text field
 * @param x top left x coordinate for text field
 * @param y top left y coordinate for text field
 * @param w width of the field
 * @param h height of the field
 * @param opts other options
 * @constructor
 */
function TextField(ID, text, x, y, w, h, opts) {
    this.ID = ID || undefined;
    this.x = x || 0;
    this.y = y || 0;
    this.w = w || 1;
    this.h = h || 1;
    this.fillStyle = opts.fillStyle || "#000000";
    this.textBaseline = opts.baseline || "middle";
    this.textColor = opts.textColor || "#000000";
    this.font = opts.font || "12px Courier";
    this.text = text || "";
    this.textHeight = opts.textHeight || 12;
    this.textAlign = opts.textAlign || "center";
    this.textMaxSpan = opts.textMaxSpan || 150;
    this.isStroke = opts.isStroke || false;

    // Visibility
    this.isVisible = true;
}


/**
 * Function constructor for simple Circle shape.
 *
 * @param centerX x value for circle center
 * @param centerY y value for circle center
 * @param radius radius of a circle
 * @param startAngle starting angle
 * @param endAngle ending angle
 * @param rotation CCW or CW rotation
 * @param color stroke color
 * @param fillStyle fill color
 * @param lineWidth width of a circle shape
 * @constructor
 */
function Circle(centerX, centerY, radius, startAngle, endAngle, rotation, color, fillStyle, lineWidth) {
    this.x = centerX;
    this.y = centerY;
    this.radius = radius;
    this.startAngle = startAngle;
    this.endAngle = endAngle;
    this.rotation = rotation;
    this.fillStyle = fillStyle;
    this.lineWidth = lineWidth;
    this.color = color;
    this.transform = (new Transform()).getDefaultTransform();

    // Visibility
    this.isVisible = true;
}


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* Draw functions for all shapes */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


/**
 * Actual drawing of shape Circle.
 *
 * @param ctx context to draw to
 */
Circle.prototype.drawShape = function (ctx) {
    ctx.save();
    ctx.strokeStyle = this.fillStyle;
    ctx.fillStyle = this.fillStyle;
    ctx.lineWidth = this.lineWidth;
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, this.startAngle, this.endAngle, this.rotation);
    ctx.stroke();
    ctx.fill();
    ctx.restore();
};

/**
 * Prototype which is used to draw this shape object.
 *
 * @param ctx context to draw to
 */
Circle.prototype.draw = function (ctx) {
    if (this.selected === true) {
        ctx.save();
        setDefaultSelection(ctx);
        this.drawShape(ctx);
        ctx.restore();
    } else {
        this.drawShape(ctx);
    }

};

/**
 * Prototype which is used to draw Line object.
 *
 * @param ctx context to draw to
 */
DLine.prototype.draw = function (ctx) {
    ctx.save();
    setupContext(ctx, this);
    ctx.beginPath();
    ctx.moveTo(this.x1, this.y1);
    ctx.lineTo(this.x2, this.y2);
    ctx.stroke();
    ctx.restore();
};


/**
 * Prototype which is used to draw this shape object.
 *
 * @param ctx context to draw to
 */
Shape.prototype.draw = function (ctx) {
    ctx.save();

    // Draw selection
    if (this.selected === true) {
        let off = 5;
        ctx.strokeStyle = this.selectionColor;
        ctx.strokeRect(this.x - off, this.y - off, this.w + 2 * off, this.h + 2 * off);
    }

    ctx.fillStyle = this.fill;
    if (this.isStroke === true) {
        ctx.strokeRect(this.x, this.y, this.w, this.h);
    } else if (this.isStroke === false) {
        ctx.fillRect(this.x, this.y, this.w, this.h);
    } else {
        ctx.rect(this.x, this.y, this.w, this.h);
        ctx.fill();
        ctx.stroke();
    }

    ctx.restore();
};

/**
 * Prototype which is used to draw Line object.
 *
 * @param ctx context to draw to
 */
Line.prototype.draw = function (ctx) {
    ctx.save();
    ctx.strokeStyle = this.fill;
    ctx.color = this.color;
    ctx.lineWidth = this.lineWidth;
    ctx.moveTo(this.x1, this.y1);
    ctx.lineTo(this.x2, this.y2);
    ctx.stroke();
    ctx.restore();
};


/**
 * Actual drawing of MyButton shape.
 *
 * @param ctx context to draw to
 */
MyButton.prototype.drawShape = function (ctx) {
    if (this.curved === false) {
        ctx.beginPath();
        ctx.rect(this.x, this.y, this.w, this.h);
        canvasStrokeAndFill(this, ctx);
    } else {
        // Add rounded path and fill it...
        ctx.beginPath();
        let r = {
            left: this.leftCurve,
            right: this.rightCurve
        };

        ctx.moveTo(this.x + r.left, this.y);
        ctx.lineTo(this.x + this.w - r.right, this.y);
        ctx.quadraticCurveTo(this.x + this.w, this.y, this.x + this.w, this.y + r.right);
        ctx.lineTo(this.x + this.w, this.y + this.h - r.right);
        ctx.quadraticCurveTo(this.x + this.w, this.y + this.h, this.x + this.w - r.right, this.y + this.h);
        ctx.lineTo(this.x + r.left, this.y + this.h);
        ctx.quadraticCurveTo(this.x, this.y + this.h, this.x, this.y + this.h - r.left);
        ctx.lineTo(this.x, this.y + r.left);
        ctx.quadraticCurveTo(this.x, this.y, this.x + r.left, this.y);

        canvasStrokeAndFill(this, ctx);
    }
};


/**
 * Actual drawing of shape TextField.
 *
 * @param ctx context to draw to
 */
TextField.prototype.drawShape = function (ctx) {
    // Set different color for text
    ctx.fillStyle = this.textColor;

    // MultiLineText
    let lines = this.text.split("\n");
    let numOfLines = lines.length;
    let lineHeight = this.textHeight;
    for (let c = 0; c < numOfLines; c++) {
        let textWidth = ctx.measureText(lines[c]).width;
        let centerX = (this.w - textWidth) / 2;
        let centerY = (this.h - this.textHeight) / 2;
        if (this.isStroke === true) {
            ctx.strokeText(lines[c], this.x + centerX, this.y + centerY + lineHeight * c + this.textHeight / 2, this.textMaxSpan);
        } else {
            ctx.fillText(lines[c], this.x + centerX, this.y + centerY + lineHeight * c + this.textHeight / 2, this.textMaxSpan);
        }
    }
};

/**
 * Prototype for drawing FrameHolder shape.
 *
 * @param ctx context to draw to
 */
FrameHolder.prototype.draw = function (ctx) {
    this.thisFrame.draw(ctx);
    this.listOfShapes.forEach((shape) => {
        ctx.beginPath();
        shape.draw(ctx);
    })
};

/**
 * Prototype which is used to draw TextField object.
 *
 * @param ctx context to draw to
 */
TextField.prototype.draw = function (ctx) {
    ctx.save();
    setupContext(ctx, this);
    this.drawShape(ctx);
    ctx.restore();
};


/**
 * Prototype which is used to draw MyButton object.
 *
 * @param ctx context to draw to
 */
MyButton.prototype.draw = function (ctx) {
    ctx.save();
    setupContext(ctx, this);
    if (this.isPressed === true) {
        setNoSelection(ctx);
        this.drawShape(ctx);
    } else if (this.isHovered === true) {
        ctx.save();
        ctx.fillStyle = convertHexToRGBA(this.fillStyle, this.alphaNonTransparent).cssString();
        this.drawShape(ctx);
        ctx.restore();
    } else {
        ctx.save();
        ctx.fillStyle = convertHexToRGBA(this.fillStyle, this.alphaTransparent).cssString();
        setNoSelection(ctx);
        this.drawShape(ctx);
        ctx.restore();
    }


    // Draw text
    setNoSelection(ctx);
    this.drawText(ctx);
    ctx.restore();
};


/**
 * Actual drawing of text for MyButton shape.
 *
 * @param ctx context to draw to
 */
MyButton.prototype.drawText = function (ctx) {
    // Set different color for text
    ctx.fillStyle = this.textColor;

    // MultiLineText
    let lines = this.text.split("\n");
    let numOfLines = lines.length;
    let offsetBetweenLines = 3;
    let lineHeight = this.textHeight + offsetBetweenLines;
    ctx.textMaxSpan = 80;

    let centerY = (this.h - numOfLines * this.textHeight) / 2;
    for (let c = 0; c < numOfLines; c++) {
        let textWidth = ctx.measureText(lines[c]).width;
        let numOfLetters = lines[c].length;
        let oneLetterUnit = textWidth / numOfLetters;

        let seenLetters = numOfLetters;
        let minOffset = 3;
        let widthOffset = 5;
        for (let c = 1; c <= numOfLetters; c++) {
            if (this.w < (minOffset + widthOffset + oneLetterUnit * c)) {
                seenLetters = c - 1;
                break;
            }
        }

        let centerX = (this.w - textWidth) / 2;

        let xText = Math.max(this.x + minOffset, minOffset + this.x + centerX);

        let textString = lines[c].slice(0, seenLetters);
        ctx.fillText(textString, xText, this.y + centerY + lineHeight * c + this.textHeight / 2, this.textMaxSpan);
    }

    ctx.stroke();
};


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* Checkers for point inside shapes */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


/**
 * Checks whether mouse is contained inside circle.
 *
 * @param bossObj reference to object which transformation is used
 * @param mx x mouse coordinate
 * @param my y mouse coordinate
 * @returns {boolean} true if contains
 */
Circle.prototype.contains = function (bossObj, mx, my) {
    let p = {x: 0, y: 0};
    let r = this.radius;
    let dx = p.x - this.x;
    let dy = p.y - this.y;
    return ((dx * dx) + (dy * dy)) < (r * r);
};

/**
 * Checks whether Shape object contains point where mouse is at.
 *
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
Shape.prototype.contains = function (mx, my) {
    return containsForRectangle(mx, my, this);
};

/**
 * Checks whether Line object contains point where mouse is at.
 *
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
Line.prototype.contains = function (mx, my) {
    return (this.x <= mx) && (this.x + this.w >= mx) &&
        (this.y <= my) && (this.y + this.h >= my);
};


/**
 * Checks whether mouse is contained inside MyButton shape.
 *
 * @param mx x mouse coordinate
 * @param my y mouse coordinate
 * @returns {boolean} true if contains
 */
MyButton.prototype.contains = function (mx, my) {
    return containsForRectangle(mx, my, this);
};

/**
 * Checks whether point is contained in FrameHolder.
 *
 * @param mx x mouse coordinate
 * @param my y mouse coordinate
 * @returns {boolean} true if contains
 */
FrameHolder.prototype.contains = function (mx, my) {
    return this.thisFrame.contains(mx, my);
};


/**
 * Checks whether TextField object contains point where mouse is at.
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
TextField.prototype.contains = function (mx, my) {
    return (this.x <= mx) && (this.x + this.w >= mx) &&
        (this.y <= my) && (this.y + this.h >= my);
};


/**
 * Helper function for check if mouse is contained in a rectangle.
 *
 * @param mx x mouse coordinate
 * @param my y mouse coordinate
 * @param o object to check
 * @returns {boolean} true if contains
 */
function containsForRectangle(mx, my, o) {
    return (mx > o.x) && (mx < ( o.x + o.w)) &&
        (my > o.y) && (my <= ( o.y + o.h));
}


/**
 * Frame holder constructor for creating holders.
 *
 * @param ID identification
 * @param frame frame to set holder to
 * @constructor
 */
function FrameHolder(ID, frame) {
    this.ID = ID || undefined;
    this.thisFrame = frame;
    this.listOfShapes = [];

    this.addShape = function (shape) {
        this.listOfShapes.push(shape);
    };

    this.getTotalWidth = function () {
        let sum = 0;
        this.listOfShapes.forEach(function (shape) {
            sum += shape.w;
        });
        return sum;
    }
}


/**
 * Prototype for getting shapes inside holder.
 *
 * @param ID identification of a shape to look for
 * @returns {undefined}
 */
FrameHolder.prototype.getShapeByID = function (ID) {
    let retVal = undefined;
    this.listOfShapes.some((shape) => {
        if (shape.ID === ID) {
            retVal = shape;
            return true;
        }
    });

    return retVal;
};

/**
 * Prototype for removing shapes inside frame holder.
 *
 * @param ID identification of a shape to remove
 */
FrameHolder.prototype.removeShapeByID = function (ID) {
    let index = this.listOfShapes.indexOf(this.getShapeByID(ID));
    if (index > -1) {
        // Adjust shapes
        let bufferShapes = this.listOfShapes;
        let previousValue = bufferShapes[index].x;
        this.listOfShapes.splice(index, 1);

        for (let c = index; c < bufferShapes.length; c++) {
            bufferShapes[c].x = previousValue;
            previousValue = bufferShapes[c].x + bufferShapes[c].w;
        }

    } else {
        /*throw "Nothin found for ID: " + ID;*/
    }
};


/**
 * Set no selection options.
 *
 * @param ctx context to set selection to
 */
function setNoSelection(ctx) {
    ctx.shadowBlur = 0;
    ctx.shadowColor = "#000000";
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
}


/**
 * Sets default selection.
 *
 * @param ctx context to set selection to
 * @param shadowBlur amount of blur for shadow
 * @param shadowColor color of shadow
 * @param offX x offset from top left corner of shape
 * @param offY y offset from top left corner of shape
 */
function setDefaultSelection(ctx, shadowBlur, shadowColor, offX, offY) {
    ctx.shadowBlur = shadowBlur || 0;
    ctx.shadowColor = shadowColor || "#000000";
    ctx.shadowOffsetX = offX || 0;
    ctx.shadowOffsetY = offY || 0;
}


/**
 * Sets context to fill and/or stroke depending on current reference given.
 *
 * @param ref reference to look for options
 * @param ctx context to change
 */
function canvasStrokeAndFill(ref, ctx) {
    if (ref.fillShape) {
        ctx.fill();
    }
    if (ref.strokeShape) {
        ctx.stroke();
    }
}


/**
 * Sets up context from given shape object.
 *
 * @param ctx context to setup (defaults to itself if any attribute for undefined shape properties)
 * @param o shape object
 */
function setupContext(ctx, o) {
    // Color and shadow options
    ctx.fillStyle = o.fillStyle || ctx.fillStyle;
    ctx.strokeStyle = o.strokeStyle || ctx.strokeStyle;
    ctx.shadowColor = o.shadowColor || ctx.shadowColor;
    ctx.shadowBlur = o.shadowBlur || ctx.shadowBlur;
    ctx.shadowOffsetX = o.shadowOffsetX || ctx.shadowOffsetX;
    ctx.shadowOffsetY = o.shadowOffsetY || ctx.shadowOffsetY;

    if (o.transform !== undefined) {
        ctx.shadowOffsetX -= o.transform.translation.x;
        ctx.shadowOffsetY -= o.transform.translation.y;
    }

    // Line style
    ctx.lineCap = o.lineCap || ctx.lineCap;
    ctx.lineWidth = o.lineWidth || ctx.lineWidth;
    ctx.setLineDash(o.lineDash || []);

    // Font properties
    ctx.font = o.font || ctx.font;
    ctx.textAlign = o.textAlign || ctx.font;
    ctx.textBaseline = o.textBaseline || ctx.textBaseline;

}

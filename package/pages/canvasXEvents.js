/**
 * Gets canvas button shapes.
 *
 * @param myState state to look for buttons.
 * @returns {{addFrame: shape, removeFrame: shape, showFrame: shape, changeFrame: shape, waitFrame: shape}}
 */
function getButtons(myState) {
    let addFrameBtn = myState.getShapeByID(myState.constants.strings.addFrameID);
    let removeFrameBtn = myState.getShapeByID(myState.constants.strings.removeFrameID);
    let showFrameBtn = myState.getShapeByID(myState.constants.strings.showFrameID);
    let changeFrameBtn = myState.getShapeByID(myState.constants.strings.changeFrameID);
    let waitFrameBtn = myState.getShapeByID(myState.constants.strings.waitFrameID);

    return {
        addFrame: addFrameBtn,
        removeFrame: removeFrameBtn,
        showFrame: showFrameBtn,
        changeFrame: changeFrameBtn,
        waitFrame: waitFrameBtn
    };
}


/**
 * Gets common frames, buffers and dynamic frame
 *
 * @param myState state to get shapes to
 * @returns {{d: shape, m: shape, f: shape, s: shape}}
 */
function getCommonObjects(myState) {
    let dynamicFrame = myState.getShapeByID(myState.constants.strings.dynamicFrame);

    let mainFrameHolder = myState.getShapeByID(myState.constants.strings.mainFrame);
    let firstBuffer = mainFrameHolder.getShapeByID(myState.constants.strings.buffer1);
    let secondBuffer = mainFrameHolder.getShapeByID(myState.constants.strings.buffer2);

    return {
        d: dynamicFrame,
        m: mainFrameHolder,
        f: firstBuffer,
        s: secondBuffer
    };
}


/**
 * Export function which converts pixels values into time values.
 *
 * @param myState state to look for shapes and frames
 * @returns {{first: Array, second: Array}}
 */
function convertToTime(myState) {
    let commons = getCommonObjects(myState);
    let mainFrame = commons.m;
    let firstBuffer = commons.f;
    let secondBuffer = commons.s;

    let expandFunc = (frame, fillList) => {
        let startValue = frame.x - mainFrame.thisFrame.x;
        let endValue = startValue + frame.w;
        let start = getTimeUnitEasy(myState, startValue);
        let end = getTimeUnitEasy(myState, endValue);
        fillList.push({
            start: start,
            end: end,
            ID: frame.text,
            options: frame.options
        });
    };


    let listBuff1 = [];
    let listBuff2 = [];
    firstBuffer.listOfShapes.forEach((frame) => {
        expandFunc(frame, listBuff1)
    });
    secondBuffer.listOfShapes.forEach((frame) => {
        expandFunc(frame, listBuff2)
    });
    return {
        first: listBuff1,
        second: listBuff2
    };
}


/**
 * Sets canvas fields to represent selected frame.
 *
 * @param myState state to set the fields to
 * @param startValue time value of starting point for selected frame
 * @param endValue time value of ending point for selected frame
 * @param delta time value of width for selected frame
 */
function setFields(myState, startValue, endValue, delta) {
    let startField = myState.getShapeByID(myState.constants.strings.startTextField);
    let endField = myState.getShapeByID(myState.constants.strings.endTextField);
    let deltaField = myState.getShapeByID(myState.constants.strings.deltaTextField);

    let startFields = startField.text.split(" ");
    let endFields = endField.text.split(" ");
    let deltaFields = deltaField.text.split(" ");
    let space = " ";
    startField.text = startFields[0] + space + (startValue) + space + startFields[2];
    endField.text = endFields[0] + space + (endValue) + space + endFields[2];
    deltaField.text = deltaFields[0] + space + (delta) + space + deltaFields[2];
}


/**
 *  Sets time values for given frame from other frame.
 *
 * @param frame frame to set values to
 * @param other frame to get values from
 */
function setTimeValuesFromFrame(frame, other) {
    frame.timeWidth = other.timeWidth;
    frame.time.startValue = other.time.startValue;
    frame.time.endValue = other.time.endValue;
}


/**
 * Sets time values for frame with specified parameters.
 *
 * @param frame frame to set values to
 * @param start start value
 * @param end end value
 * @param delta time width value
 */
function setTimeValuesForFrame(frame, start, end, delta) {
    frame.timeWidth = delta;
    frame.time.startValue = start;
    frame.time.endValue = end;
}


/**
 * Function to check moving a frame.
 *
 * @param myState state to look frames for
 * @param mainFrameHolder frame holder for buffers
 * @param dynamicFrame dynamic frame which is being move
 * @param firstBuffer buffer for frames
 * @param secondBuffer buffer for frames
 * @param mouse current mouse position
 */
function eventMoveDynamic(myState, mainFrameHolder, dynamicFrame, firstBuffer, secondBuffer, mouse) {
    if (!firstBuffer.contains(mouse.x, mouse.y) && !secondBuffer.contains(mouse.x, mouse.y)) {
        dynamicFrame.isVisible = false;
        return;
    }

    // Set new parameters for frame, if there are already some skip them
    // Setting startnig position by calculating origin
    let sumX = 0;

    let getSum = function (buffer) {
        let numOfBufferElements = buffer.listOfShapes.length;
        if (numOfBufferElements <= 0) {
            return mainFrameHolder.thisFrame.x;
        }
        let lastFrame = buffer.listOfShapes[numOfBufferElements - 1];
        return (lastFrame.x + lastFrame.w);
    };
    if (firstBuffer.contains(mouse.x, mouse.y)) {
        sumX += getSum(firstBuffer);
        dynamicFrame.y = firstBuffer.thisFrame.y;
        dynamicFrame.h = firstBuffer.thisFrame.h;
    } else if (secondBuffer.contains(mouse.x, mouse.y)) {
        sumX += getSum(secondBuffer);
        dynamicFrame.y = secondBuffer.thisFrame.y;
        dynamicFrame.h = secondBuffer.thisFrame.h;
    }

    dynamicFrame.x = sumX;
    // Set width of a frame
    let dx = mouse.x - sumX;

    let frameMin = myState.constants.numeric.frameMinWidth;
    let newWidth = Math.max(frameMin, dx);
    let endOfBuffer = mainFrameHolder.thisFrame.x + mainFrameHolder.thisFrame.w;

    if (mouse.x < (endOfBuffer) && (sumX + newWidth) < (endOfBuffer)) {
        let bufferStartX = mouse.x - mainFrameHolder.thisFrame.x;
        dynamicFrame.w = newWidth;
        let startValue = getTimeUnitEasy(myState, sumX - mainFrameHolder.thisFrame.x);
        let endValue = getTimeUnitEasy(myState, bufferStartX);
        let deltaValue = endValue - startValue;
        let frameMinTime = getTimeUnitEasy(myState, frameMin);
        if (newWidth <= frameMin) {
            let newStart = getTimeUnitEasy(myState, sumX);
            setTimeValuesForFrame(dynamicFrame, newStart, newStart + frameMinTime, frameMinTime);
            setFields(myState, newStart, newStart + frameMinTime, frameMinTime);
        } else {
            setTimeValuesForFrame(dynamicFrame, startValue, endValue, deltaValue);
            setFields(myState, startValue, endValue, deltaValue);
        }

        dynamicFrame.textMaxSpan = Math.max(myState.constants.numeric.minTextSpan, newWidth);
        dynamicFrame.options.textMaxSpan = Math.max(myState.constants.numeric.minTextSpan, newWidth);


        setDynamicFrameText(myState, dynamicFrame);
        // Set frame to visible only if there is something to draw, frame must be at least 1 pixel long.
        dynamicFrame.isVisible = true;
        // Set approriate start, end and delta fields values
    } else {
        dynamicFrame.isVisible = false;
    }
}


/**
 * Checks other frames which needs adjusting after a move of another frame.
 *
 * @param myState state to get objects from
 * @param bufferShapes shapes to change
 * @param index from where to
 */
function propagateMoveChange(myState, bufferShapes, index) {
    let mainFrame = getCommonObjects(myState).m;
    let previousValue = bufferShapes[index].x + bufferShapes[index].w;
    for (let c = index + 1; c < bufferShapes.length; c++) {
        bufferShapes[c].x = previousValue;
        previousValue = bufferShapes[c].x + bufferShapes[c].w;
        bufferShapes[c].time.endValue = getTimeUnitEasy(myState, bufferShapes[c].x + bufferShapes[c].w - mainFrame.thisFrame.x);
        bufferShapes[c].time.startValue = getTimeUnitEasy(myState, bufferShapes[c].x - mainFrame.thisFrame.x);
        bufferShapes[c].timeWidth = bufferShapes[c].time.endValue - bufferShapes[c].time.startValue;
    }
}


/**
 * Event for moving frame.
 *
 * @param myState state to get objects from
 * @param mainFrameHolder frame holder for buffers
 * @param firstBuffer buffer for frames
 * @param secondBuffer buffer for frames
 * @param mouse current mouse position
 */
function eventMoveFrame(myState, mainFrameHolder, firstBuffer, secondBuffer, mouse) {
    let idx = myState.changeSetup.shapeIndex;
    let whichBuff = myState.changeSetup.which;

    if (idx === -1 || whichBuff === 0) {
        /*throw "Not a valid index or buffer value!" + "which is:" + whichBuff
         + "\n index is: " + idx*/
        return;
    }

    if (!(firstBuffer.contains(mouse.x, mouse.y) && whichBuff === 1) &&
        !(secondBuffer.contains(mouse.x, mouse.y) && whichBuff === 2)) {
        return;
    }
    let shapes = (whichBuff === 1) ? firstBuffer.listOfShapes : secondBuffer.listOfShapes;
    let changingFrame = shapes[idx];


    // Calculate widths after changing frame
    let sumOfWidths = function (shapes, idx) {
        let sum = 0;
        let shapeLenght = shapes.length;
        for (let c = idx + 1; c < shapeLenght; c++) {
            sum += shapes[c].w;
        }
        return sum;
    }(shapes, idx);

    let dx = mouse.x - changingFrame.x;
    if ((changingFrame.x + dx + sumOfWidths) < (mainFrameHolder.thisFrame.w + mainFrameHolder.thisFrame.x)) {
        let frameMin = myState.constants.numeric.frameMinWidth;
        let newWidth = Math.max(frameMin, dx);

        changingFrame.w = newWidth;

        let bufferEndX = mouse.x - mainFrameHolder.thisFrame.x;
        let bufferStartX = changingFrame.x - mainFrameHolder.thisFrame.x;
        let startValue = getTimeUnitEasy(myState, bufferStartX);
        let endValue = getTimeUnitEasy(myState, bufferEndX);
        let deltaValue = endValue - startValue;
        let frameMinTime = getTimeUnitEasy(myState, frameMin);

        if (newWidth <= frameMin) {
            let previousStart = sumOfWidths - changingFrame.x;
            setTimeValuesForFrame(changingFrame,
                previousStart, startValue + frameMinTime, frameMinTime);
            setFields(myState, startValue, startValue + frameMinTime, frameMinTime);
        } else {
            setTimeValuesForFrame(changingFrame, startValue, endValue, deltaValue);
            setFields(myState, startValue, endValue, deltaValue);
        }

        setNewLength(changingFrame, changingFrame.timeWidth);
        changingFrame.textMaxSpan = Math.max(myState.constants.numeric.minTextSpan, newWidth);
        changingFrame.options.textMaxSpan = Math.max(myState.constants.numeric.minTextSpan, newWidth);


        // Now that we changed our frame, we need to propagate this change
        propagateMoveChange(myState, shapes, idx);
        checkForVisibleLines(myState, whichBuff);
    }
}


/**
 * Gets different color based on given frame identification.
 *
 * @param myState state to get colors from
 * @param frame frame to check identity
 * @returns {string} hex color
 */
function getColorForFrameID(myState, frame) {
    let frameTitle = frame.text.split("\n")[1];
    if (frame.text.startsWith(myState.constants.strings.waitFrame)) {
        return myState.constants.colors.buffers.waitFrame;
    } else if (frameTitle.startsWith(myState.constants.strings.showFrame)) {
        return myState.constants.colors.buffers.showFrame;
    } else {
        return myState.constants.colors.buffers.writeFrame;
    }
}


/**
 * Event for setting frame selection.
 *
 * @param myState state to look frames for
 * @param dynamicFrame dynamic frame which is being move
 * @param firstBuffer buffer for frames
 * @param secondBuffer buffer for frames
 * @param mouse current mouse position
 */
function eventMoveSelect(myState, dynamicFrame, firstBuffer, secondBuffer, mouse) {
    dynamicFrame.isVisible = false;
    let nothingHovered = true;
    // Check for selected frames
    let setFrameSelections = function (buffer) {
        buffer.listOfShapes.some((frame) => {
            frame.isHovered = frame.contains(mouse.x, mouse.y) === true;
            if (frame.isHovered === true) {
                setFields(myState, frame.time.startValue, frame.time.endValue, frame.timeWidth);
                nothingHovered = false;
                return false;
            }
        });
    };
    setFrameSelections(firstBuffer);
    setFrameSelections(secondBuffer);
    if (nothingHovered === true) {
        setFields(myState, 0, 0, 0);
    }

}


/**
 * Checks selection based on mouse position.
 *
 * @param myState state to get frame buttons from
 * @param mouse current mouse position
 */
function checkSelection(myState, mouse) {
    let buttons = getButtons(myState);
    let addFrameBtn = buttons.addFrame;
    let removeFrameBtn = buttons.removeFrame;
    let waitFrameBtn = buttons.waitFrame;
    let changeFrameBtn = buttons.changeFrame;
    let showFrameBtn = buttons.showFrame;

    let selectionsChange = false;
    let oldSelections = [addFrameBtn.isHovered, removeFrameBtn.isHovered, showFrameBtn.isHovered, changeFrameBtn.isHovered, waitFrameBtn.isHovered];

    addFrameBtn.isHovered = addFrameBtn.contains(mouse.x, mouse.y) === true;
    removeFrameBtn.isHovered = removeFrameBtn.contains(mouse.x, mouse.y) === true;
    showFrameBtn.isHovered = showFrameBtn.contains(mouse.x, mouse.y) === true;
    changeFrameBtn.isHovered = changeFrameBtn.contains(mouse.x, mouse.y) === true;
    waitFrameBtn.isHovered = waitFrameBtn.contains(mouse.x, mouse.y) === true;

    let newSelections = [addFrameBtn.isHovered, removeFrameBtn.isHovered, showFrameBtn.isHovered, changeFrameBtn.isHovered, waitFrameBtn.isHovered];
    let size = oldSelections.length;
    for (let c = 0; c < size; c++) {
        if (oldSelections[c] !== newSelections[c]) {
            selectionsChange = true;
            break;
        }
    }

    // Frames events
    let commons = getCommonObjects(myState);
    let dynamicFrame = commons.d;
    let mainFrameHolder = commons.m;
    let firstBuffer = commons.f;
    let secondBuffer = commons.s;

    let revalidate = true;
    if (!mainFrameHolder.contains(mouse.x, mouse.y)) {
        dynamicFrame.isVisible = false;
        revalidate = false;
    }
    switch (myState.selectedBtn) {
        case "showFrame":
        case "waitFrame":
        case "addFrame":
            eventMoveDynamic(myState, mainFrameHolder, dynamicFrame, firstBuffer, secondBuffer, mouse);
            break;
        case "changeFrame":
            if (myState.changeSetup.changeFlag === true) {
                // Do job and exit...
                eventMoveFrame(myState, mainFrameHolder, firstBuffer, secondBuffer, mouse);
                break;
            }
        // Fallthrough
        case "removeFrame":
            eventMoveSelect(myState, dynamicFrame, firstBuffer, secondBuffer, mouse);
            break;
        default:
            // Check frame selection
            eventMoveSelect(myState, dynamicFrame, firstBuffer, secondBuffer, mouse);
            revalidate = true;
    }


    if (revalidate === true || selectionsChange === true) {
        myState.invalidate();
    }

}


/**
 * Mouse move event.
 *
 * @param e event object
 * @param myState canvas state
 */
function mouseMoved(e, myState) {
    let canvas = myState.canvas;
    let mouse = getMouse(canvas, e);
    // Set mouse tracker values:
    myState.mouseX = Math.round(mouse.x);
    myState.mouseY = Math.round(mouse.y);
    myState.mouseLabel = "mx: " + myState.mouseX + ",  my: " + myState.mouseY;

    checkSelection(myState, mouse);

}


/**
 * Helper function to hide time lines from buffers.
 *
 * @param myState state for which the check is provided
 * @param which buffer index, which buffer to check for
 */
function checkForVisibleLines(myState, which) {
    let listOfLines;
    let mainFrame = myState.getShapeByID(myState.constants.strings.mainFrame);
    let lastFrame;
    let shapes;
    switch (which) {
        case 1:
            shapes = mainFrame.getShapeByID(myState.constants.strings.buffer1).listOfShapes;
            lastFrame = shapes[shapes.length - 1] || {x: -1, w: -1};

            listOfLines = myState.getShapesByGroupID("linesBuff1");
            listOfLines.forEach((line) => {
                line.isVisible = line.x1 > (lastFrame.x + lastFrame.w);
            });
            break;

        case 2:
            shapes = mainFrame.getShapeByID(myState.constants.strings.buffer2).listOfShapes;
            lastFrame = shapes[shapes.length - 1] || {x: -1, w: -1};

            listOfLines = myState.getShapesByGroupID("linesBuff2");
            listOfLines.forEach((line) => {
                line.isVisible = line.x1 > (lastFrame.x + lastFrame.w);
            });
            break;
    }
}


/**
 * Helper function to set frame color to.
 *
 * @param frame frame to set color to
 * @param color color to set
 */
function setFrameColor(frame, color) {
    frame.fillStyle = color;
    frame.options.fillStyle = color;
}


/**
 * Snaps width of a frame to natural position.
 *
 * @param state state to get unit conversion constants of
 * @param frame frame to snap width
 */
function snapWidthDynamic(state, frame) {
    let endValue = frame.time.endValue;
    let timeUnit = state.constants.numeric.timeUnit;
    let pixelUnit = state.constants.numeric.pixelUnit;
    let offValue = getCommonObjects(state).m.thisFrame.x;
    let oldWidth = frame.w;
    if ((endValue % timeUnit) === 0) {
        let dx = (frame.x + frame.w - offValue) % (pixelUnit);
        frame.w = frame.w - (dx);
    }

    if (frame.w > oldWidth) {
        frame.w = oldWidth;
    }
}


/**
 * Helper function for snapping specific changing frame frame.
 *
 * @param state state to get unit conversion constants of
 * @param frame frame to snap width
 */
function snapWidthChanging(state, frame) {
    let endValue = frame.time.endValue;
    let timeUnit = state.constants.numeric.timeUnit;
    let oldWidth = frame.w;
    if ((endValue % timeUnit) === 0) {
        let startValue = frame.time.startValue;
        let delta = endValue - startValue;
        frame.w = getPixelUnitEasy(state, delta);
    }

    if (frame.w > oldWidth) {
        frame.w = oldWidth;
    }
}


/**
 * Event for adding frame to a buffer.
 *
 * @param myState state reference
 * @param whichText text to apply to a frame
 * @param firstBuffer first buffer
 * @param secondBuffer second buffer
 * @param mouse current mouse position
 */
function eventAddFrame(myState, whichText, firstBuffer, secondBuffer, mouse) {
    let dynamicFrame = myState.getShapeByID(myState.constants.strings.dynamicFrame);

    if (!dynamicFrame.isVisible) {
        return;
    }

    // Check if any buffer is clicked
    // Create new frame from dynamic frame
    // Get the frame index
    let whichBuff = (firstBuffer.contains(mouse.x, mouse.y) === true) ? 1 :
        (secondBuffer.contains(mouse.x, mouse.y) === true) ? 2 : 0;

    if (whichBuff === 0) {
        dynamicFrame.isVisible = false;
        return;
    }

    let frameIndex;
    let framePrefix;
    let frameText = "";

    if (whichText === 0) {
        frameIndex = dynamicFrame.options.showFrameIndex;

        dynamicFrame.text = frameText + frameIndex + "\n" + myState.constants.strings.showFrame + "\n" + dynamicFrame.timeWidth;
        dynamicFrame.options.showFrameIndex =
            ((dynamicFrame.options.showFrameIndex + 1) === 5) ? 1 : dynamicFrame.options.showFrameIndex + 1;

        framePrefix = myState.constants.strings.showFrame;
    } else if (whichText === 1) {
        frameIndex = dynamicFrame.options.waitFrameIndex;

        dynamicFrame.text = myState.constants.strings.waitFrame + "\n" + dynamicFrame.timeWidth;
        dynamicFrame.options.waitFrameIndex += 1;

        framePrefix = myState.constants.strings.waitFrame
    } else {
        frameIndex = dynamicFrame.options.addFrameIndex;

        dynamicFrame.text = frameText + frameIndex + "\n" + myState.constants.strings.addFrame + "\n" + dynamicFrame.timeWidth;
        dynamicFrame.options.addFrameIndex =
            ((dynamicFrame.options.addFrameIndex + 1) === 5) ? 1 : dynamicFrame.options.addFrameIndex + 1;

        framePrefix = myState.constants.strings.addFrame;
    }

    let frameID = framePrefix + frameIndex + createUID(frameIndex);
    let newOpts = cloneParentObj(dynamicFrame.options);

    snapWidthDynamic(myState, dynamicFrame);
    let newFrame = new MyButton(frameID,
        dynamicFrame.x, dynamicFrame.y, dynamicFrame.w, dynamicFrame.h, dynamicFrame.text, newOpts);

    newFrame.options.frameIndex = frameIndex;

    let newFrameColor = getColorForFrameID(myState, newFrame);
    setFrameColor(newFrame, newFrameColor);

    setTimeValuesFromFrame(newFrame, dynamicFrame);
    newFrame.isVisible = true;

    if (whichBuff === 1) {
        firstBuffer.addShape(newFrame);
        checkForVisibleLines(myState, 1);
    } else if (whichBuff === 2) {
        secondBuffer.addShape(newFrame);
        checkForVisibleLines(myState, 2);
    }
    dynamicFrame.isVisible = false;
}


/**
 * Adjust frame indexes in case of delete, or index change
 *
 * @param myState state reference
 */
function adjustFrameIndexes(myState) {
    let commons = getCommonObjects(myState);
    // Find used indexes for write frame and show frame

    let findMaxForBuffer = function (buffer, which) {
        let max = -1;
        buffer.listOfShapes.forEach((shape) => {
            let arr = shape.text.split("\n");
            let firstLine = arr[0];
            if (firstLine.startsWith(myState.constants.strings.waitFrame)) {
                // Skip wait frame
            } else {
                if (arr[1].startsWith(which)) {
                    max = Math.max(shape.options.frameIndex, max);
                } else {
                    // Skip not wanted frames...
                }
            }
        });
        return max;
    };

    let findMax = function (which) {
        return Math.max(findMaxForBuffer(commons.f, which), findMaxForBuffer(commons.s, which));
    };

    let showMaxIdx = findMax(myState.constants.strings.showFrame);
    let writeMaxIdx = findMax(myState.constants.strings.addFrame);

    commons.d.options.showFrameIndex = showMaxIdx + 1;
    commons.d.options.addFrameIndex = (writeMaxIdx === -1) ? 1 : writeMaxIdx + 1;

    if (commons.d.options.showFrameIndex >= 5) {
        commons.d.options.showFrameIndex = 1;
    }
    if (commons.d.options.addFrameIndex >= 5) {
        commons.d.options.addFrameIndex = 1;
    }
}


/**
 * Event for moving frame.
 *
 * @param myState state reference
 * @param firstBuffer first buffer
 * @param secondBuffer second buffer
 * @param mouse current mouse position
 */
function eventRemoveFrame(myState, firstBuffer, secondBuffer, mouse) {
    let removeSelectedFrame = function (buffer) {
        buffer.listOfShapes.some((frame) => {
            if (frame.isHovered === true) {
                buffer.removeShapeByID(frame.ID);
                adjustFrameIndexes(myState);
                return true;
            }
        });
    };

    if (firstBuffer.contains(mouse.x, mouse.y)) {
        removeSelectedFrame(firstBuffer);
        checkForVisibleLines(myState, 1);
    } else if (secondBuffer.contains(mouse.x, mouse.y)) {
        removeSelectedFrame(secondBuffer);
        checkForVisibleLines(myState, 2);
    }
}


/**
 * Snaps all frames in a buffer.
 *
 * @param myState state reference
 * @param buff buffer to transverse
 */
function snapper(myState, buff) {
    buff.listOfShapes.forEach((sh) => {
        snapWidthChanging(myState, sh);
    });
}


/**
 * Event for changing frame.
 *
 * @param myState state reference
 * @param firstBuffer first buffer
 * @param secondBuffer second buffer
 * @param mouse current mouse position
 */
function eventChangeFrame(myState, firstBuffer, secondBuffer, mouse) {
    if (myState.changeSetup.changeFlag === true) {
        myState.changeSetup.changeFlag = false;

        if (myState.changeSetup.which === 1) {
            snapper(myState, firstBuffer);
            propagateMoveChange(myState, firstBuffer.listOfShapes, myState.changeSetup.shapeIndex);
        } else if (myState.changeSetup.which === 2) {
            snapper(myState, secondBuffer);
            propagateMoveChange(myState, secondBuffer.listOfShapes, myState.changeSetup.shapeIndex);
        }

        return;
    }

    let setShapeIndex = function (buffer) {

        myState.changeSetup = {
            changeFlag: false,
            shapeIndex: -1,
            which: 0,
            initialWidth: 1,
            frame: null
        };

        let shapes = buffer.listOfShapes;
        let framesLength = shapes.length;
        for (let c = 0; c < framesLength; c++) {
            if (shapes[c].isHovered === true) {
                myState.changeSetup.shapeIndex = c;
                myState.changeSetup.initialWidth = shapes[c].w;
                myState.changeSetup.changeFlag = true;
                myState.changeSetup.frame = shapes[c];
                break;
            }
        }
    };

    if (firstBuffer.contains(mouse.x, mouse.y)) {
        setShapeIndex(firstBuffer);
        myState.changeSetup.which = 1;
    } else if (secondBuffer.contains(mouse.x, mouse.y)) {
        setShapeIndex(secondBuffer);
        myState.changeSetup.which = 2;
    }
}


/**
 * Helper function for setting frame text.
 *
 * @param myState reference to state
 * @param dynamicFrame reference to dynamic frame
 */
function setDynamicFrameText(myState, dynamicFrame) {
    let frameIndex;
    switch (myState.selectedBtn) {
        case "addFrame":
            frameIndex = dynamicFrame.options.addFrameIndex;
            dynamicFrame.text = frameIndex + "\n" + myState.constants.strings.addFrame + "\n" + dynamicFrame.timeWidth;
            break;
        case "waitFrame":
            dynamicFrame.text = myState.constants.strings.waitFrame + "\n" + dynamicFrame.timeWidth;
            break;
        case "showFrame":
            frameIndex = dynamicFrame.options.showFrameIndex;
            dynamicFrame.text = frameIndex + "\n" + myState.constants.strings.showFrame + "\n" + dynamicFrame.timeWidth;
            break;
    }
}


/**
 * Hot click checker.
 *
 * @param myState reference to state
 * @param mouse current mouse position
 * @returns {boolean} false if nothing is applicable to this mouse position
 */
function checkClicks(myState, mouse) {
    let invalidate = true;
    let frames = getCommonObjects(myState);

    let gotClick = false;
    let radioButtons = myState.getShapesByGroupID(myState.constants.strings.groupOptionsBtns);
    radioButtons.some((btn) => {
            let isPressed = btn.contains(mouse.x, mouse.y) === true;
            if (isPressed) {
                btn.isPressed = btn.isPressed === false;
                if (btn.isPressed === false) {
                    btn.transform.translation.y = 0;
                    myState.selectedBtn = "none";
                } else {
                    myState.selectedBtn = btn.ID;
                    btn.transform.translation.y = 2;
                    setDynamicFrameText(myState, frames.d);
                }
                gotClick = true;
                return true;
            } else if (mouse.y < 50) {
                if (btn.isPressed === true) {
                    btn.isPressed = false;
                    myState.selectedBtn = "none";
                    btn.transform.translation.y = 0;
                }
            }
        }
    );

    // Clear others
    if (gotClick === true) {
        radioButtons.forEach((btn) => {
            if (btn.ID !== myState.selectedBtn) {
                btn.transform.translation.y = 0;
                btn.isPressed = false;
            }
        });
        myState.invalidate();
        return true;
    }

    let whichText;
    switch (myState.selectedBtn) {
        case "showFrame":
            whichText = 0;
            break;
        case "waitFrame":
            whichText = 1;
            break;
        default:
            whichText = 2;
    }

    let removeSelection = true;
    let changeFrameClick = false;
    switch (myState.selectedBtn) {
        case "showFrame":
        case "waitFrame":
        case "addFrame":
            eventAddFrame(myState, whichText, frames.f, frames.s, mouse);
            break;
        case "removeFrame":
            eventRemoveFrame(myState, frames.f, frames.s, mouse);
            break;
        case "changeFrame":
            eventChangeFrame(myState, frames.f, frames.s, mouse);
            changeFrameClick = true;
            break;
        default:
            invalidate = false;
            removeSelection = false;
    }

    if (changeFrameClick === false) {
        myState.changeSetup.changeFlag = false;
    }
    if (invalidate === true) {
        myState.invalidate();
        return true;
    }

    return false;
}


/**
 * Checks mouse hovering on frames.
 *
 * @param e event object
 * @param myState reference to state
 * @param mouse current mouse position
 */
function checkForFrame(e, myState, mouse) {
    let commons = getCommonObjects(myState);
    // Check for selected frames
    let setFrameSelections = function (buffer, which) {
        buffer.listOfShapes.some((frame, index) => {
            frame.isHovered = frame.contains(mouse.x, mouse.y) === true;
            if (frame.isHovered === true) {
                myState.xOptions.gObj.showModal(e, myState, frame, which, index);
                return false;
            }
        });
    };
    setFrameSelections(commons.f, 1);
    setFrameSelections(commons.s, 2);
}


/**
 *  Checks for right click on canvas.
 *
 * @param myState reference to state
 */
function checkRightClick(myState) {
    myState.selectedBtn = "none";
    let radioButtons = myState.getShapesByGroupID(myState.constants.strings.groupOptionsBtns);
    radioButtons.forEach((btn) => {
        btn.isPressed = false;
        btn.transform.translation.y = 0;
    });

    myState.getShapeByID(myState.constants.strings.dynamicFrame).isVisible = false;
    myState.invalidate();
}


/**
 * Event dispatcher for clicks.
 * @param ev event object
 * @param myState reference to state
 */
function mouseClick(ev, myState) {
    let canvas = myState.canvas;
    let mouseStd = getMouse(canvas, ev);

    let mouse = {
        x: mouseStd.x,
        y: mouseStd.y
    };

    let isRightClick;
    let e = ev || window.event;

    if ("which" in e) {
        isRightClick = e.which === 3;
    } else if ("button" in e) {
        isRightClick = e.button === 2;
    }

    if (isRightClick === true) {
        // No right click because of tablets...
        checkRightClick(myState);
    } else {
        if (checkClicks(myState, mouse) === false) {
            // Modal dialog, is mouse under frame?
            checkForFrame(e, myState, mouse);
        }
    }
}


/**
 * Event for mouse release.
 *
 * @param e event object
 * @param myState reference to state
 */
function mouseReleased(e, myState) {
    if (myState.dragg) {
        myState.dragg = false;
    }

    // Remove selection
    myState.invalidate();
}


/**
 * Event for key down.
 *
 * @param e event object
 * @param myState reference to state
 */
function keyDownPress(e, myState) {
    let x = e.which || event.keyCode;
    let code = myState.constants.keyCodes;

    let successful = false;
    if (successful) {
        e.preventDefault();
    }

    myState.invalidate();
}


/**
 * Event for key press.
 *
 * @param e event object
 * @param myState reference to state
 */
function keyPress(e, myState) {
    let x = e.which || event.keyCode;
    // let y = String.fromCharCode(x).toUpperCase();

    let successful = false;
    if (successful) {
        e.preventDefault();
    }
    myState.invalidate();
}
/**
 * Transforms hex number (base 16) to decimal (base 10).
 *
 * @param hexNumber hex number to convert to decimal
 * @returns {Number} converted number
 */
function hexToDecimal(hexNumber) {
    return parseInt(hexNumber, 16);
}


/**
 * Function constructor for RGBA color values.
 *
 * @param red amount of red
 * @param green amount of green
 * @param blue amount of blue
 * @param alpha amount of transparency
 * @constructor
 */
function RGBA(red, green, blue, alpha) {
    this.red = red;
    this.green = green;
    this.blue = blue;
    this.alpha = alpha;
    this.cssString = function () {
        return "rgba(" + this.red + "," + this.green + "," + this.blue + "," + this.alpha + ")"
    }
}


/**
 * Converts hex color to rgba.
 *
 * @param hexColor color to convert
 * @param alpha alpha value for new rgba color
 * @returns {RGBA} rgba representation of hex color
 */
function convertHexToRGBA(hexColor, alpha) {
    let red = hexToDecimal(hexColor.slice(1, 3));
    let green = hexToDecimal(hexColor.slice(3, 5));
    let blue = hexToDecimal(hexColor.slice(5, 7));
    return new RGBA(red, green, blue, alpha);
}


/**
 * Sets html canvas properties from constants.
 *
 * @param canvas canvas for which properties are set
 * @param constants constants
 */
function setCanvasAttributes(canvas, constants) {
    canvas.width = constants.numeric.canvasWidth;
    canvas.height = constants.numeric.canvasHeight;
}


/**
 * Sets new frame length.
 *
 * @param frame frame to set length to
 * @param newLength length to set
 */
function setNewLength(frame, newLength) {
    let oldArr = frame.text.split("\n");
    let size = oldArr.length;
    let strBuild = "";

    for (let c = 0; c < (size - 1); c++) {
        strBuild += (oldArr[c] + "\n");
    }

    frame.text = strBuild + newLength;
}


/**
 * Sets new index and length for a frame.
 *
 * @param myState reference to state
 * @param frame frame to set length and index to
 * @param newIndex new index value
 * @param newLength new length
 */
function setNewIndexAndLength(myState, frame, newIndex, newLength) {
    let tmpIndex;
    let idx;
    if (frame.text.startsWith(myState.constants.strings.waitFrame)) {
        tmpIndex = "";
        idx = 0;
    } else {
        tmpIndex = newIndex + "\n";
        idx = 1;
    }
    let oldArr = frame.text.split("\n");
    frame.text = tmpIndex + oldArr[idx] + "\n" + newLength;
    frame.w = getPixelUnitEasy(myState, newLength);
    frame.options.frameIndex = newIndex;

    let endValue = frame.time.startValue + newLength;
    setTimeValuesForFrame(frame, frame.time.startValue, endValue, newLength);
    snapWidthDynamic(myState, frame);
}


/**
 * Helper function for closing dialog by identification.
 *
 * @param ID id of an element to close
 */
function closeDialogByID(ID) {
    closeDivByID(ID);
}


/**
 * Helper function to open dialog by identification.
 *
 * @param ID id of an element to show
 */
function openDialogByID(ID) {
    showDivByID(ID);
}


/**
 * Shows div tag element with specified identification.
 *
 * @param ID id of an element to show
 */
function showDivByID(ID) {
    document.getElementById(ID).style.display = "block";
}


/**
 * Hides div tag element with specified identification.
 *
 * @param ID id of an element to hide
 */
function closeDivByID(ID) {
    document.getElementById(ID).style.display = "none";
}


/**
 * Sets focus for element specified by identification.
 *
 * @param ID id of an element to set focus to
 */
function setFocusById(ID) {
    document.getElementById(ID).focus();
}


/**
 * Clones parent to new object.
 *
 * @param parent object to copy
 * @returns {{}}
 */
function cloneParentObj(parent) {
    let child = {};
    for (let key in parent) {
        if (parent.hasOwnProperty(key)) {
            child[key] = parent[key];
        }
    }

    return child;
}


/**
 * Creates unique ID of with a specified number.
 *
 * @param helpNum number to create ID of
 * @returns {string} unique ID
 */
function createUID(helpNum) {
    return (Math.random() * 0x10000 * 42 * 31 + helpNum).toString(16).slice(2, 6);
}
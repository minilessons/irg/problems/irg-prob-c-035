/*@#include#(constants.js)*/
/*@#common-include#(commonUtil.js)*/

/*@#include#(canvasX.js)*/
/*@#include#(canvasProto.js)*/
/*@#include#(canvasShapes.js)*/
/*@#include#(canvasXEvents.js)*/
/*@#include#(taskInit.js)*/
/*@#include#(frontEndUtil.js)*/

/**
 * @external questionLocalID
 * @type {string}
 */
let canId = questionLocalID + "_buff1";
let canIdSync = questionLocalID + "_buff2";

let modId = questionLocalID + "_modalDialog";
let formId = questionLocalID + "_boxForm";
let valId = questionLocalID + "_boxValue";
let lengthValueId = questionLocalID + "_lengthBoxValue";
let inputBoxId = questionLocalID + "_inputBoxId";
let dimModalId = questionLocalID + "_dimModal";
let myDim = questionLocalID + "_myDim";
let innerWarningTextId = questionLocalID + "_innerModalText";

let canConst;
let canvasX;
let canvasXSync;

/**
 * Defines global object which is exported and used in HTML document.
 */
let gObj = {

    /* Togglers */
    toggleElement: function (id) {
        let elem = document.getElementById(id);
        if (elem.style.display === "none") {
            showDivByID(id);
        } else {
            closeDivByID(id);
        }
    },

    myStateRef: null,

    closeDimDialog: function () {
        closeDialogByID(dimModalId);
        openDialogByID(modId);
    },
    /**
     * Closes modal dialog.
     */
    closeDialog: function () {
        closeDialogByID(modId);
        closeDivByID(myDim);
    },

    showWarningModal: function (msg) {
        document.getElementById(innerWarningTextId).innerText = msg;
        openDialogByID(dimModalId);
        showDivByID(myDim);
        closeDialogByID(modId);

    },

    /**
     * Shows dialog for user input and set foucs on it.
     */
    openEntryDialog: function () {
        openDialogByID(modId);
        setFocusById(inputBoxId);
    },


    /**
     * Validates user input before sending it into canvas.
     * @returns {boolean} always false because of form submit.
     */
    validateForm: function () {
        let formVal = Number(document.forms[formId][valId].value);
        let formLengthValue = Number(document.forms[formId][lengthValueId].value);
        let frameMinTime = getTimeUnitEasy(this.myStateRef, this.myStateRef.constants.numeric.frameMinWidth);

        if (formLengthValue < frameMinTime) {
            let msg = this.myStateRef.constants.strings.minimalWidthWarning + frameMinTime;
            this.showWarningModal(msg);
            return false;
        }

        if (formVal >= 0 && formVal < 6) {
            // Check if with this new width it will exceed our buffer
            let aCheck = function (myState, newWidth, frame, which) {
                let frames = getCommonObjects(myState);
                let sum = 0;
                if (which === 1) {
                    sum = frames.f.getTotalWidth();
                } else if (which === 2) {
                    sum = frames.s.getTotalWidth();
                } else {
                    /*throw "No Such Buffer!, for which: " + which;*/
                    return false;
                }

                sum += (getPixelUnitEasy(myState, newWidth) - frame.w);
                let allowedWidth = frames.m.thisFrame.w;
                return sum < allowedWidth;
            };

            if (aCheck(this.myStateRef, formLengthValue, this.selectedFrame, this.which)
                === false) {
                let msg = this.myStateRef.constants.strings.bufferWidthWarning;
                this.showWarningModal(msg);
                return false;
            }

            // Set new width, time values, propagate change and snap width;
            if (this.myStateRef !== null) {
                setNewIndexAndLength(this.myStateRef, this.selectedFrame, formVal, formLengthValue);

                this.selectedFrame.w = getPixelUnitEasy(this.myStateRef, formLengthValue);
                let commons = getCommonObjects(this.myStateRef);
                if (this.which === 1) {
                    snapper(this.myStateRef, commons.f);
                    propagateMoveChange(this.myStateRef, commons.f.listOfShapes, this.frameIdx);
                    checkForVisibleLines(this.myStateRef, this.which);
                } else if (this.which === 2) {
                    snapper(this.myStateRef, commons.s);
                    propagateMoveChange(this.myStateRef, commons.s.listOfShapes, this.frameIdx);
                    checkForVisibleLines(this.myStateRef, this.which);
                }

                this.myStateRef.invalidate();
            } else {
                /*throw "Null reference error!";*/
            }
            this.closeDialog();
        }
        return false;
    },


    /**
     * Event callback for modal dialog.
     * Shows prepared modal dialog to user if user clicked right mouse button on pixel.
     * @param canvas canvas which the user clicked
     * @param clickedFrame frame for which the dialog is opened
     * @param e passed event object
     * @param which which buffer this frame belongs to (must be 1 or 2)
     */
    showModal: function (e, canvas, clickedFrame, which, idx) {
        this.myStateRef = canvas;
        this.selectedFrame = clickedFrame;
        this.which = which;
        this.frameIdx = idx;

        let idxValue;
        if (clickedFrame.text.startsWith(canvas.constants.strings.waitFrame)) {
            // Disable index entry
            document.getElementById(inputBoxId).readOnly = true;
            idxValue = "";
        } else {
            idxValue = clickedFrame.options.frameIndex;
        }

        let setupVal = idxValue;
        let setupLength = clickedFrame.timeWidth;

        document.forms[formId][valId].value = setupVal;
        document.forms[formId][lengthValueId].value = setupLength;
        this.openEntryDialog();
        showDivByID(myDim);
    }
};


/**
 * Global Export with name "gObj"
 *
 * @external qExportGlobalObject
 */
qExportGlobalObject(questionLocalID, 'gObj', gObj);

/**
 * Api used to communicate between html and backend data stream
 * @type {{initQuestion: api.initQuestion, takeState: api.takeState, resetState: api.resetState, revertState: api.revertState}}
 */
let api = {
    /**
     * Initializes question with user state and common state
     *
     * @param cst common state object
     * @param userState user data object
     */
    initQuestion: function (cst, userState) {
        /**
         * @external readOnly
         */
        if (readOnly === true) {
            gObj.toggleElement(questionLocalID + "_htuDesc");
            gObj.toggleElement(questionLocalID + "_taskDesc");
        }

        let canvas = document.getElementById(canId);
        let canvasSync = document.getElementById(canIdSync);

        canConst = getQuestionConstants(cst.qs.myConstants);
        setCanvasAttributes(canvas, canConst);
        setCanvasAttributes(canvasSync, canConst);
        let xOptions = {
            initialSetup: userState.qs.withoutSync,
            readOnly: readOnly,
            enableText: true,
            gObj: gObj,
            htmlIds: {
                empty: 0
            },
        };

        let xOptionsSync = {
            initialSetup: userState.qs.withSync,
            readOnly: readOnly,
            enableText: true,
            gObj: gObj,
            htmlIds: {
                empty: 0
            },
        };

        canvasX = new CanvasX(canvas, xOptions, canConst);
        canvasXSync = new CanvasX(canvasSync, xOptionsSync, canConst);

        let someLocale = cst.qs.localeID;
        canvasX.setLocale(someLocale);
        canvasXSync.setLocale(someLocale);

        initTaskDrawings(canvasX);
        initTaskDrawings(canvasXSync);
        canvasX.invalidate();
        canvasXSync.invalidate();


    },

    /**
     *  Export function for taking current and last user entered data
     *
     * @returns {{withSync: ({first, second}|*), withoutSync: ({first, second}|*)}}
     */
    takeState: function () {
        return {
            withSync: canvasXSync.exportState(),
            withoutSync: canvasX.exportState()
        };

    },

    /**
     * Reset function for task redoing.
     *
     * @param cst common state
     * @param emptyState empty state to reset to
     */
    resetState: function (cst, emptyState) {
        canvasX.resetCanvas(emptyState.qs.withoutSync);
        canvasXSync.resetCanvas(emptyState.qs.withSync);
        initTaskDrawings(canvasX);
        initTaskDrawings(canvasXSync);
        canvasX.invalidate();
        canvasXSync.invalidate();
    },

    /**
     * Reverts the question state to a state which was loaded when page loaded.
     *
     * @param cst common state
     * @param loadedState state at the time of page load
     */
    revertState: function (cst, loadedState) {
        // Reset state to loaded transformation matrix.
        canvasX.resetCanvas(loadedState.qs.withoutSync);
        canvasXSync.resetCanvas(loadedState.qs.withSync);
        initTaskDrawings(canvasX);
        initTaskDrawings(canvasXSync);
        canvasX.invalidate();
        canvasXSync.invalidate();

    }
};

// noinspection JSAnnotator
return api;
/**
 * Task initialisation: creates all shapes and adds them to canvas
 *
 * @param can canvas to set shapes to
 */
function initTaskDrawings(can) {
    let frameWidth = 50;

    let xOff = can.ctx.measureText(can.constants.strings.bufferName1).width - 13;
    let bufferColors = can.constants.colors.buffers.background;
    let mainFrameColor = can.constants.colors.buffers.mainFrame;
    let mainFrameY = 50;
    let mainFrameWidth = 800;
    let mainFrameX = Math.floor(xOff + (can.canvas.width - mainFrameWidth) / 2);
    let mainFrameHeight = 200;
    let buffHeight = 80;
    let buffOffset = 20;
    let frameOffset = 10;

    let bothStrokeAndFill = 3;
    let onlyStroke = true;
    let mainFrame = new Shape(mainFrameX, mainFrameY, mainFrameWidth, mainFrameHeight, mainFrameColor, false, "mainFrame", onlyStroke);
    let firstBuff = new Shape(mainFrameX, mainFrameY + frameOffset, mainFrameWidth, buffHeight, bufferColors, false, "firstBuff", bothStrokeAndFill);
    let secondBuff = new Shape(mainFrameX, firstBuff.y + buffHeight + buffOffset, mainFrameWidth, buffHeight, bufferColors, false, "secondBuff", bothStrokeAndFill);

    let opts = {
        fillStyle: can.constants.colors.buffers.innerFramesAdding,
        alphaTransparent: can.constants.colors.frames.transparent,
        alphaNonTransparent: can.constants.colors.frames.nonTransparent,
        strokeStyle: can.constants.colors.black,
        font: can.constants.fonts.normalFont,
        textColor: can.constants.colors.buffers.innerText,
        textMaxSpan: frameWidth,
        shadowBlur: 0,
        shadowOffsetX: 0,
        shadowOffsetY: 0,
        shadowColor: can.constants.colors.shadowFrame,
        showFrameIndex: 1,
        addFrameIndex: 1,
        waitFrameIndex: 0,
        frameIndex: 0,
        leftCurve: 12,
        rightCurve: 12,
        curved: false,
        selectionColor: can.constants.colors.frames.selectionColor,
        dropShadowOnHover: true,
        strokeShape: true,
        fillShape: true
    };

    let dynamicFrame = new MyButton("dynamicFrame", 50, 50, frameWidth, 30, "blank", opts);

    dynamicFrame.isVisible = false;


    // Check if frameHolder is valid
    let mainFrameHolder = new FrameHolder(can.constants.strings.mainFrame, mainFrame);
    let firstBuffer = new FrameHolder(can.constants.strings.buffer1, firstBuff);
    let secondBuffer = new FrameHolder(can.constants.strings.buffer2, secondBuff);

    // Set color for predefined buffers
    setInitialBufferFrames(firstBuffer, can, can.xOptions.initialSetup.first, mainFrameHolder.thisFrame.x, opts);
    setInitialBufferFrames(secondBuffer, can, can.xOptions.initialSetup.second, mainFrameHolder.thisFrame.x, opts);


    mainFrameHolder.addShape(firstBuffer);
    mainFrameHolder.addShape(secondBuffer);

    can.addShape(mainFrameHolder);
    can.addShape(dynamicFrame);

    // For saveing and restoring indexes in buffers!
    adjustFrameIndexes(can);

    createButtons(can);
    createLines(can);
    createFrameStatusBar(can);
    createBufferName(can, firstBuffer, can.constants.strings.bufferName1);
    createBufferName(can, secondBuffer, can.constants.strings.bufferName2);

    checkForVisibleLines(can, 1);
    checkForVisibleLines(can, 2);

}


/**
 * Sets initial setup for buffer frames.
 *
 * @param buff buffer to set to
 * @param can canvas reference
 * @param aList list to get initial setup for a buffer
 * @param initialOffset offset from canvas left point
 * @param opts other frame options
 */
function setInitialBufferFrames(buff, can, aList, initialOffset, opts) {
    aList.forEach((oldFrame, index) => {
        let width = oldFrame.end - oldFrame.start;
        let x = getPixelUnitEasy(can, oldFrame.start);
        let pixelWidth = getPixelUnitEasy(can, width);

        let newID;
        let text = oldFrame.ID.split("\n");
        if (text[0].startsWith(can.constants.strings.waitFrame)) {
            newID = text[0];
        } else {
            newID = text[1];
        }

        let newOpts = opts;
        if (oldFrame.options !== undefined) {
            newOpts = cloneParentObj(oldFrame.options);
        }

        let newFrame = new MyButton(newID + createUID(index), x + initialOffset, buff.thisFrame.y, pixelWidth, buff.thisFrame.h, oldFrame.ID, newOpts);

        setTimeValuesForFrame(newFrame, oldFrame.start, oldFrame.end, width);
        let frameTitle = newFrame.text.split("\n")[1];
        if (newFrame.text.startsWith(can.constants.strings.waitFrame)) {
            setFrameColor(newFrame, can.constants.colors.buffers.waitFrame);
        } else if (frameTitle.startsWith(can.constants.strings.showFrame)) {
            setFrameColor(newFrame, can.constants.colors.buffers.showFrame);
        } else {
            setFrameColor(newFrame, can.constants.colors.buffers.writeFrame);
        }
        buff.addShape(newFrame);
    });
}


/**
 * Helper function to create needed lines on canvas.
 *
 * @param canvas canvas to add lines to
 */
function createLines(canvas) {
    let mainFrame = canvas.getShapeByID(canvas.constants.strings.mainFrame);
    let buff1 = mainFrame.getShapeByID(canvas.constants.strings.buffer1);
    let buff2 = mainFrame.getShapeByID(canvas.constants.strings.buffer2);

    let distanceX = canvas.constants.numeric.pixelUnit;
    let bufferWidth = mainFrame.thisFrame.w;
    let yBuff1 = buff1.thisFrame.y;
    let yBuff2 = buff2.thisFrame.y;
    let buffHeight = buff1.thisFrame.h;
    let numOfLines = Math.floor(bufferWidth / distanceX) - 1;

    let timeUnit = canvas.constants.numeric.timeUnit;
    let tickW = 20;
    let tickH = 20;

    let opts = {
        fillStyle: canvas.constants.colors.black,
        lineWidth: 1,
        lineDash: [8, 4],
    };

    let textOpts = {
        fillStyle: "#dDabc2",
        textBaseline: "middle",
        textColor: "#000000",
        font: "12px Courier",
        textHeight: 12,
        textAlign: "center",
        textMaxSpan: tickW * 2,
        isStroke: true
    };

    let startX = mainFrame.thisFrame.x;
    let groupID1 = "linesBuff1";
    for (let c = 0; c < numOfLines; c++) {
        let currentX = startX + (c + 1) * distanceX;
        let line =
            new DLine("line" + c, currentX,
                yBuff1, startX + (c + 1) * distanceX,
                yBuff1 + buffHeight, opts, groupID1);
        canvas.addShape(line);

        // Add number
        let tickText = " " + ((c + 1 ) * timeUnit);
        canvas.addShape(new TextField("tick" + c, tickText, currentX, yBuff1 + buffHeight, tickW, tickH, textOpts));
    }

    let groupID2 = "linesBuff2";
    for (let c = 0; c < numOfLines; c++) {
        let line =
            new DLine("line" + numOfLines + c, startX + (c + 1) * distanceX,
                yBuff2, startX + (c + 1) * distanceX,
                yBuff2 + buffHeight, opts, groupID2);
        canvas.addShape(line);
    }

    let tickOffset = 6;
    for (let c = 0; c < numOfLines; c++) {
        let line =
            new DLine("lineTick" + c, startX + (c + 1) * distanceX,
                yBuff1 - tickOffset, startX + (c + 1) * distanceX,
                yBuff1, opts);
        canvas.addShape(line);
    }

    for (let c = 0; c < numOfLines; c++) {
        let line =
            new DLine("lineTick" + numOfLines + c, startX + (c + 1) * distanceX,
                yBuff2 - tickOffset / 2, startX + (c + 1) * distanceX,
                yBuff2, opts);
        canvas.addShape(line);
    }
}


/**
 * Text fields for buffer names.
 *
 * @param canvas canvas to add fields to
 * @param buffer buffer to set name to
 * @param bufferName name of a buffer
 */
function createBufferName(canvas, buffer, bufferName) {
    let xStart = 40;
    let yStart = buffer.thisFrame.y;
    let buffWidth = 100;
    let buffHeight = buffer.thisFrame.h;

    let opts = {
        font: canvas.constants.fonts.middleFont,
        textColor: canvas.constants.colors.black,
        fillStyle: "#1234DD",
        isStroke: false,
        textHeight: 17,
        textMaxSpan: 2 * buffWidth,
        textAlign: "center"
    };

    let bufferField = new TextField("textField" + createUID(31) + buffer.thisFrame.ID, bufferName, xStart, yStart, buffWidth, buffHeight, opts);
    canvas.addShape(bufferField);
}


/**
 * Creates status bar with few text fields.
 *
 * @param canvas canvas to add fields to.
 */
function createFrameStatusBar(canvas) {
    let mainFrameHolder = canvas.getShapeByID(canvas.constants.strings.mainFrame);
    let yFrameOffset = 10;
    let fieldWidth = 70;
    let fieldHeight = 20;
    let fieldOffset = 80;
    let numOfFields = 3;
    let startX = mainFrameHolder.thisFrame.x + mainFrameHolder.thisFrame.w -
        (numOfFields * fieldWidth + fieldOffset * (numOfFields - 1));
    let startY = mainFrameHolder.thisFrame.y + mainFrameHolder.thisFrame.h + yFrameOffset;
    let fontSize = 20;

    let options = {
        font: canvas.constants.fonts.biggerFont,
        textColor: canvas.constants.colors.black,
        fillStyle: "#1234DD",
        isStroke: false,
        textHeight: fontSize,
        textMaxSpan: 2 * fieldWidth,
        textAlign: "center"
    };

    let locale = canvas.getLocale();

    let unit = "ms";
    let startFieldValue = 0;
    let endFieldValue = 0;
    let differenceFieldValue = endFieldValue - startFieldValue;
    let startField = new TextField(canvas.constants.strings.startTextField,
        locale.startString + ": " + startFieldValue + " " + unit, startX, startY, fieldWidth, fieldHeight, options);
    let endField = new TextField(canvas.constants.strings.endTextField,
        locale.endString + ": " + endFieldValue + " " + unit, startX + fieldWidth + fieldOffset, startY, fieldWidth, fieldHeight, options);
    let deltaField = new TextField(canvas.constants.strings.deltaTextField,
        locale.deltaString + ": " + differenceFieldValue + " " + unit,
        startX + 2 * fieldWidth + 2 * fieldOffset, startY, fieldWidth, fieldHeight, options);

    canvas.addShape(startField);
    canvas.addShape(endField);
    canvas.addShape(deltaField);
}


/**
 * Helper function for creating buttons.
 *
 * @param canvas canvas to add buttons to
 */
function createButtons(canvas) {
    let mainFrameHolder = canvas.getShapeByID(canvas.constants.strings.mainFrame);
    let radioOffset = 15;
    let btnWidth = 100;
    let btnHeight = 25;
    let numOfBtns = 5;
    let leftAlignment = numOfBtns * btnWidth + radioOffset * (numOfBtns - 1);
    let startRadioX = mainFrameHolder.thisFrame.x + mainFrameHolder.thisFrame.w - (leftAlignment);
    let dropOffset = 3;
    let startRadiosY = mainFrameHolder.thisFrame.y - btnHeight - dropOffset;

    let curveConst = 5;
    let opts = {
        fillStyle: canvas.constants.colors.btns.lightTeal,
        alphaTransparent: canvas.constants.colors.btns.transparent,
        alphaNonTransparent: canvas.constants.colors.btns.nonTransparent,
        font: canvas.constants.fonts.normalFont,
        textColor: canvas.constants.colors.btns.textBlack,
        textMaxSpan: btnWidth,
        shadowBlur: 0,
        shadowOffsetX: 0,
        shadowOffsetY: 0,
        shadowColor: canvas.constants.colors.shadowBtns,
        curved: true,
        leftCurve: curveConst,
        rightCurve: curveConst,
        textBaseline: "middle",
        dropShadowOnHover: true,
        fillShape: true,
        strokeShape: false
    };

    let locale = canvas.getLocale();

    let addFrameBtn = new MyButton(canvas.constants.strings.addFrameID,
        startRadioX, startRadiosY, btnWidth, btnHeight, locale.addFrame, opts);
    let removeFrameBtn = new MyButton(canvas.constants.strings.removeFrameID,
        btnWidth + startRadioX + radioOffset, startRadiosY, btnWidth, btnHeight, locale.removeFrame, opts);
    let waitFrameBtn = new MyButton(canvas.constants.strings.waitFrameID,
        startRadioX + 2 * btnWidth + 2 * radioOffset, startRadiosY, btnWidth, btnHeight, locale.waitFrame, opts);
    let showFrameBtn = new MyButton(canvas.constants.strings.showFrameID, startRadioX + 3 * btnWidth + 3 * radioOffset, startRadiosY, btnWidth, btnHeight, locale.showFrame, opts);
    let changeFrameBtn = new MyButton(canvas.constants.strings.changeFrameID, startRadioX + 4 * btnWidth + 4 * radioOffset, startRadiosY, btnWidth, btnHeight,
        locale.changeFrame, opts);

    let btnGroupID = canvas.constants.strings.groupOptionsBtns;
    addFrameBtn.groupID = btnGroupID;
    removeFrameBtn.groupID = btnGroupID;
    waitFrameBtn.groupID = btnGroupID;
    showFrameBtn.groupID = btnGroupID;
    changeFrameBtn.groupID = btnGroupID;

    canvas.addShape(addFrameBtn);
    canvas.addShape(removeFrameBtn);
    canvas.addShape(waitFrameBtn);
    canvas.addShape(showFrameBtn);
    canvas.addShape(changeFrameBtn);
}
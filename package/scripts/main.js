/*@#include#(serverUtil.js)*/
/*@#common-include#(commonUtil.js)*/


/* Main Module
 * Contains backend functions which server side calls in order to setup and evaluate task; it
 * also deals with data stream between frontend and backend.
 */

/**
 * Initializer functions for question. Uses configuration file for setting some variables.
 * @param questionConfig configuration JSON object provided in "config-default.json" file.
 */
function questionInitialize(questionConfig) {
    // init task configuration (see: config.json file for details)
    let timeUnit = questionConfig.timeUnit;
    let pixelUnit = questionConfig.pixelUnit;
    let syncFactor = questionConfig.syncFactor;
    let noSyncFactor = questionConfig.noSyncFactor;
    let localeID = questionConfig.localeLanguage;
    let precision = questionConfig.resultPrecision;

    // Canvas attributes
    let canWidth = questionConfig.canvasWidth;
    let canHeight = questionConfig.canvasHeight;

    // Create random values for four buffers
    let minimalOffet = 6;
    let minimalTime = 10;
    let randSwitch = randNum(0, 5);
    let firstVal;
    let thirdVal;
    if (randSwitch > 2) {
        firstVal = randNum(25, 29);
        thirdVal = randNum(25, 29);
    } else {
        //firstVal = randNum(32, 35);
        //thirdVal = randNum(32, 35);
        firstVal = randNum(30, 2 * timeUnit - minimalOffet);
        thirdVal = randNum(30, 2 * timeUnit - minimalOffet);
    }
    let secondVal = randNum(minimalTime, timeUnit - minimalOffet);
    let fourthVal = randNum(minimalTime, timeUnit - minimalOffet);

    // Saving information for question and other data which will be sent to front-end

    let waitFrameValue = 0;
    if (firstVal > 20) {
        waitFrameValue = 40 - firstVal;
    }

    let initialState = fillInitial(firstVal, waitFrameValue);

    /**
     * @external userData
     */
    userData.question = {
        questionState: initialState

    };
    userData.myConstants = {
        firstValue: firstVal,
        secondValue: secondVal,
        thirdValue: thirdVal,
        fourthValue: fourthVal,
        syncFactor: syncFactor,
        noSyncFactor: noSyncFactor,
        pixelUnit: pixelUnit,
        timeUnit: timeUnit,
        localeID: localeID,
        resultPrecision: precision,
        canWidth: canWidth,
        canHeight: canHeight,
        firstVal: firstVal,
        waitFrameValue: waitFrameValue
    };
    userData.correctQuestionState = {
        withSync: getEmptyBuffers(),
        withoutSync: getEmptyBuffers()
    };

}


/**
 * Returns computed properties which can be used in frontend as echo directives
 * @returns {{x0, y0: *, x1, y1: *}} coordinates of question start point and end point
 */
function getComputedProperties() {
    return {
        t1: userData.myConstants.firstValue,
        t2: userData.myConstants.secondValue,
        t3: userData.myConstants.thirdValue,
        t4: userData.myConstants.fourthValue,
    };
}


/**
 * Question evaluator for this question.
 * @returns {{correctness: number, solved: boolean}}
 */
function questionEvaluate() {
    let correctResult = calculateCorrectResults(userData.myConstants, false);
    let correctResultSync = calculateCorrectResults(userData.myConstants, true);


    /**
     * Sets correct solution if user did not answer the question 100%.
     */
    function setCorrectSolution() {
        userData.correctQuestionState = {withSync: correctResultSync, withoutSync: correctResult};
    }


    let res = {correctness: 0.0, solved: false};

    let tasksBuffers = userData.question.questionState;

    let buffersWithSync = tasksBuffers.withSync;
    let buffersNoSync = tasksBuffers.withoutSync;

    if (buffersNoSync.first.length === 0 && buffersNoSync.second.length === 0 &&
        buffersWithSync.first.length === 0 && buffersWithSync.second.length === 0) {
        res.solved = false;
        res.correctness = 0.0;
        setCorrectSolution();
        return res;
    }

    res.solved = true;
    // Evaluate first buffers with no sync
    let grade1 = compareResults(correctResult, buffersNoSync);
    let grade2 = compareResults(correctResultSync, buffersWithSync);

    let syncFactor = userData.myConstants.syncFactor;
    let noSyncFactor = userData.myConstants.noSyncFactor;
    let finalGrade = noSyncFactor * grade1 + syncFactor * grade2;
    res.correctness = Number(Math.max(0.0, finalGrade).toFixed(userData.myConstants.resultPrecision));

    if (res.correctness < 1.0) {
        setCorrectSolution();
    } else {
        userData.correctQuestionState = userData.question.questionState;
    }

    return res;
}


/**
 * Exports user state and question information which frontend basic question setup needs
 *
 * @returns Object which have common specification for frontend
 */
function exportCommonState() {
    return {
        qs: {
            myConstants: userData.myConstants
        }
    };
}


/**
 * Exports question state depending on type asked
 * @param stateType can be user or correct solution
 * @returns {*}
 */
function exportUserState(stateType) {
    if (stateType === "USER") {
        return {qs: userData.question.questionState};
    }
    if (stateType === "CORRECT") {
        return {qs: userData.correctQuestionState};
    }

    return {};
}


/**
 * Exports empty state of user data question state
 * @returns Object state which is a state which has identity matrix defined
 */
function exportEmptyState() {
    return {
        qs: fillInitial(userData.myConstants.firstVal, userData.myConstants.waitFrameValue)
    }
}


/**
 * Imports question state data which is changed by the user in front end and saves it for Database.
 * @param data question state data; question state should be of the same form as exported one
 */
function importQuestionState(data) {
    userData.question.questionState = data
}
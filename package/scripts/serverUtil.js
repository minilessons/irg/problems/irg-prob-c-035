/**
 * Returns a random number between min (inclusive) and max (inclusive)
 */
function randNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}


/**
 * Gets empty buffers.
 *
 * @returns {{first: Array, second: Array}} empty arrays
 */
function getEmptyBuffers() {
    return {
        first: [], second: []
    };
}


/**
 * Fills initial frames.
 *
 * @param firstVal value of start point for first frame inside buffer
 * @param waitFrameValue value of a wait frame length
 * @returns {{withSync: {first: [null], second: Array}, withoutSync: {first: [null], second: Array}}}
 */
function fillInitial(firstVal, waitFrameValue) {
    let withSync = {
        first: [
            {
                start: 0,
                end: firstVal + waitFrameValue,
                ID: "0\nPrikaži\n" + (firstVal + waitFrameValue)
            }
        ],
        second: []
    };
    let withoutSync = {
        first: [
            {
                start: 0,
                end: firstVal,
                ID: "0\nPrikaži\n" + firstVal
            }
        ],
        second: []
    };

    return {
        withSync: withSync,
        withoutSync: withoutSync
    };
}


/**
 * Helper function for adding wait frame to a list of buffer frames.
 *
 * @param endValue end value of wait frame
 * @param list list to add frame to
 * @param timeUnit time length of wait frame
 * @returns {number}
 */
function addWait(endValue, list, timeUnit) {

    let endForWait = 0;
    if ((endValue % timeUnit) !== 0) {
        let factor = Math.ceil(endValue / timeUnit);
        endForWait = timeUnit * factor;
        let waitText = "Čekaj\n" + (endForWait - endValue);
        addToList(list, endValue, endForWait, waitText);
    }
    return endForWait - endValue;
}


/**
 * Helper function to add frame to a list.
 *
 * @param aList list to add frame to
 * @param start start value of a frame
 * @param end end value of a frame
 * @param ID identification of a frame
 */
function addToList(aList, start, end, ID) {
    aList.push({start: Math.floor(start), end: Math.floor(end), ID: ID});
}


/**
 * Helper function to calculate correct results.
 *
 * @param bufferTimes randomly generated times for buffers
 * @param withSync flag for getting result for synced frames or non synced frames
 * @returns {{first: Array, second: Array}} lists of correct order and identification for buffers
 */
function calculateCorrectResults(bufferTimes, withSync) {
    let listFirst = [];
    let listSecond = [];
    let timeUnit = bufferTimes.timeUnit;
    let pixelUnit = bufferTimes.pixelUnit;
    // We need five iterations for loop
    let arrOfTimes = [bufferTimes.firstValue, bufferTimes.secondValue, bufferTimes.thirdValue, bufferTimes.fourthValue];
    let size = arrOfTimes.length + 1;

    let texts = ["Upiši\n", "Prikaži\n"];

    let showIndex = 0;
    let writeIndex = 1;
    let accumulatedStart = 0;
    for (let c = 0; c < size; c++) {

        let endValue = arrOfTimes[c % 4];
        let text = texts[(c + 1) % 2];
        let text2 = texts[c % 2];

        let indexForFirst = (text === texts[1]) ? showIndex : writeIndex;
        let indexForSecond = (text2 === texts[1]) ? showIndex : writeIndex;

        let start = accumulatedStart;
        if (withSync === true) {

            // Check for wait frame
            if (text === texts[0]) {
                let delta = (calcDelta(start, accumulatedStart, endValue, timeUnit, pixelUnit));
                let firstText = indexForFirst + "\n" + text + (delta);
                addToList(listFirst, start, accumulatedStart + endValue, firstText);
                accumulatedStart += addWait(accumulatedStart + endValue, listFirst, timeUnit);
                delta = (calcDelta(start, accumulatedStart, endValue, timeUnit, pixelUnit));
                let secondText = indexForSecond + "\n" + text2 + delta;
                addToList(listSecond, start, accumulatedStart + endValue, secondText);
            } else if (text2 === texts[0]) {
                let delta = (calcDelta(start, accumulatedStart, endValue, timeUnit, pixelUnit));
                let secondText = indexForSecond + "\n" + text2 + delta;
                addToList(listSecond, start, accumulatedStart + endValue, secondText);
                accumulatedStart += addWait(accumulatedStart + endValue, listSecond, timeUnit);
                delta = (calcDelta(start, accumulatedStart, endValue, timeUnit, pixelUnit));
                let firstText = indexForFirst + "\n" + text + (delta);
                addToList(listFirst, start, accumulatedStart + endValue, firstText);
            }
        } else {
            let delta = (calcDelta(start, accumulatedStart, endValue, timeUnit, pixelUnit));
            let secondText = indexForSecond + "\n" + text2 + delta;
            let firstText = indexForFirst + "\n" + text + (delta);
            addToList(listFirst, start, accumulatedStart + endValue, firstText);
            addToList(listSecond, start, accumulatedStart + endValue, secondText);

        }
        accumulatedStart += endValue;
        showIndex += 1;
        writeIndex += 1;

        if (writeIndex > 4) {
            writeIndex = 1;
        }
    }

    return {
        first: listFirst,
        second: listSecond
    }
}


/**
 * Helper function to calculate delta, based on current accumulated value and start and end points.
 *
 * @param start start value
 * @param accumulated accumulated value
 * @param end end value
 * @returns {number}
 */
function calcDelta(start, accumulated, end) {
    return (accumulated + end - start);
}


/**
 * Helper function to compare buffers.
 *
 * @param first first buffer
 * @param second second buffer
 * @returns {number}
 */
function compareBuffers(first, second) {
    let correctUnits = 0;
    let smallerLength = Math.min(first.length, second.length);
    for (let c = 0; c < smallerLength; c++) {
        if (first[c].start === second[c].start &&
            first[c].end === second[c].end) {
            // Time for this is correct
            if (first[c].ID === second[c].ID) {
                // Whole unit is correct
                correctUnits += 1;
            }
        }
    }
    return correctUnits;
}


/**
 * Helper function to compare results.
 *
 * @param correct correct solution
 * @param user user solution
 * @returns {number}
 */
function compareResults(correct, user) {
    // Check first buffer
    let correctUnits = 0;

    let firstCorrect = correct.first;
    let firstUser = user.first;
    let secondCorrect = correct.second;
    let secondUser = user.second;

    correctUnits += compareBuffers(firstCorrect, firstUser);
    correctUnits += compareBuffers(secondCorrect, secondUser);

    let numOfCorrect = firstCorrect.length + secondCorrect.length;

    return correctUnits / numOfCorrect;

}